# Optimized implementation of the A* (AStar) algorithm.

## Features:

- Fast data structures: Binary Heap backed by a fast and simple hashtable for the open list. Same fast hashtable implementation used for closed list and storing obstacles
- Low memory consumption: full node obejects (with scores, parents, etc.) are only created for currently explored cells. On a 255 X 255 cells map, the memory useage is around 10 Mb.
- Also has flood option, that calculates all possible paths (from a given starting points to all reachable destinations) for a given map, using Dijkstra algorithm.

## Demos:

- 120 x 120 cells map, obstacles are not editable (can only add / remove start / destination): https://lazersoft.neocities.org/jsFastAstar/benchmark/BenchmarkOnPredefinedMap.html
- 255 x 255 cells map, can (also) add and remove obstacles: https://lazersoft.neocities.org/jsFastAstar/benchmark/InteractiveBenchmark.html

## Quick example:

```javascript
var aStarMap = new SHIVANSITE.AStarMap(120, 60); 
aStarMap.setObstacle(5, 3);
aStarMap.setObstacle(4, 3);
aStarMap.setObstacle(4, 4);
//...
var distanceCalculator = new SHIVANSITE.AStarChebyshevDistanceCalculator();
var aStar = new SHIVANSITE.AStar();
var path = aStar.getPath(3, 5, 24, 51, aStarMap,  distanceCalculator);
if(path.reachesDestination) {
    console.log("Destination was reached");
} else {
    console.log("Only partial path calculated");
}
console.log("Path calculation took: " + path.computeTimeMs + " milliseconds, and explored: " + path.exploredNodesCount + " cells");
for(var i = 0; i < path.nodes.length; i++) {
    var node = path.nodes[i];
    console.log("Path step " + i + ": cell at x=" + node.x + " y=" + node.y);
}
```

## Detailed usage:

 - <strong>Initialize the A* map.</strong> Set it to a specific size, by specifying the width and height (number of cell columns and rows). The maximum allowed value is 255 x 255. Example:

```javascript 
 var aStarMap = new SHIVANSITE.AStarMap(120, 60); 
```

The above code will create a map with 120 columns of cells by 60 rows of cells.

 - <strong>Specify which cells on the map are obstacles</strong> (a path cannot go through them, and hence, must avoid them). Example:
 
Set a specific cell as obstacle:

```javascript
 aStarMap.setObstacle(5, 3);
``` 
 
The above code will declare cell at coordinates x = 5 (column 5) and y = 3 (row 3) as obstacle.

You can change your mind and remove an obstacle from a cell:

```javascript
 aStarMap.clearObstacle(5, 3);
```

The above code will declare cell at coordinates x = 5 (column 5) and y = 3 (row 3) as not being an obstacle (it's passable: i.e. a path can pass through it). 

You can also check if a cell is declared as obstacle or not:

```javascript
 if(aStarMap.isObstacle(5, 3)) {
     console.log("This cell is an impassable obstacle");
 }
``` 
 
There's also a convenience method to set multiple cells at a time as obstacles:

```javascript 
  aStarMap.setObstacles(10, 20, 15, 30);
```  
  
The above code will set all cells in the rectancle having corners at (10, 20) and (15, 30) to obstacles.

 - <strong>Instantiate the default distance calculator</strong> (or one of your own):

```javascript
var distanceCalculator = new SHIVANSITE.AStarChebyshevDistanceCalculator();
```

The distance calculator is used internally by the A* implementation. It has 2 main roles, each fulfilled by its following functions:

```javascript
getDistance: function(n1, n2)
```

 The n1 and n2 arguments represent A* nodes: elements containing information about one cell on the map, such as cell coordinates (x, y), various A* scores (g, h, f),
 and the parent node to which this node is attached.
 
 This function returns the distance between the coordinates of the 2 nodes, when no obstacles exist between them.
 
 For the AStarChebyshevDistanceCalculator implementation, the default value returned by this function uses an approximation of the Octile Distance special case. 
 This can be customized by setting different values for the D1 and D2 parameters in the function constructor.
 For a usage where there are only 8 directions of movement from once cell to the other (4 orthogonal + 4 diagonal), the Octile Distance is good enough and sufficiently fast to compute.
 
```javascript
getNeighbors: function(node, aStarMap)
``` 
 This function generates an array of Node objects representing the neighbors of the currently passed node. There can be up to 8 of them, but can be less if the node is on the edge/corner 
 of the map, or adjacent to an obstacle cell. The returned nodes also contain the calculated g score: the distance from the current node to each of its neighbors.
 
 - <strong>Instantiate and use the AStar object.</strong>
 
This object can be interrogated multiple times for path between 2 sets of cell coordinates (start and end), on a given AStarMap instance, using a given Distance Calculator instance.
 
Here is a simple example:

```javascript
var aStar = new SHIVANSITE.AStar();
var path = aStar.getPath(3, 5, 24, 51, aStarMap,  distanceCalculator);
```

The above example calculates the shortest path between the cell located at x=3, y=5 (column 3, row 5) and the cell located at x=24 and y=51. 
It will use the obstacles declared in the "aStarMap" instance passed in the 5th argument, and the distance calculator that was passed in the 6th argument.

A 7th argument can be passed, in the form of a number representing the maximum number of nodes (cells) the algorithm will explore in search of reaching the destination via the 
shortest path, before stopping. This can be usefull especially in the situations where the path to calculate might be towards an unreachable destination (a cell "fenced" by obstacles).
In such a situation, not giving any limit on the number of explored cells will make the algorithm explore every reachable cell before stopping.

```javascript
var path = aStar.getPath(3, 5, 24, 51, aStarMap,  100);
```
The above example tryes to find the shortest path between the cell located at x=3, y=5 (column 3, row 5) and the cell located at x=24 and y=51. If after exploring 100 cells it 
hasn't arrived at the destination, it will stop. The returned path will be to the closest cell to the destination that was found amongst the 100 cells explored.

- <strong>Use the returned "path" object to get information about the shortest path:</strong>

The structure of the "path" object is the following:

```javascript
SHIVANSITE.AStarPath = function(...) {
	this.nodes = nodes;
	this.reachesDestination = reachesDestination;
	this.computeTimeMs = computeTimeMs;
	this.exploredNodesCount = exploredNodesCount;
};
```

Here's an example on how to use a SHIVANSITE.AStarPath instance returned by the "aStar" object:

```javascript
var aStar = new SHIVANSITE.AStar();
var path = aStar.getPath(3, 5, 24, 51, aStarMap,  distanceCalculator);

if(path.reachesDestination) {
    console.log("Destination was reached");
} else {
    console.log("Only partial path calculated");
}

console.log("Path calculation took: " + path.computeTimeMs + " milliseconds, and explored: " + path.exploredNodesCount + " cells");

for(var i = 0; i < path.nodes.length; i++) {
    var node = path.nodes[i];
    console.log("Path step " + i + ": cell at x=" + node.x + " y=" + node.y);
}
```
 
## Flooding a map to get all paths from a given start position to all reachable cells

The SHIVANSITE.AStar object has another public method, called "flood". Calling this method will return an object of type SHIVANSITE.AStarFloodResult, that will be able to give you the
shortest paths from a given starting point to all reachable cells.

Here's the demo page for this: https://lazersoft.neocities.org/jsFastAstar/benchmark/InteractiveFloodBenchmark.html

Here's a code example:

```javascript
var startX = 10;
var startY = 12;

var floodResult = aStar.flood(startX, startY, aStarMap, distanceCalculator);

var endX1 = 30;
var endY1 = 34;
var path1 = floodResult.getPath(endX1, endY1);

var endX2 = 20;
var endY2 = 23;
var path2 = floodResult.getPath(endX2, endY2);

//...
```

A flood calculation will take substantially less time than calculating all paths from a given start point to all reachable cells via individual A*. 


 
 
