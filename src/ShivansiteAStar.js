var SHIVANSITE = SHIVANSITE = SHIVANSITE || {};

/**
 * @class
 * @Returns {SHIVANSITE.AStar}
 */
SHIVANSITE.AStar = function() {
	
	/**
	 * @private
	 */
	this.aStarMap = null;
	
	/**
	 * @private
	 */
	this.distanceCalculator = null;
	
	/**
	 * @private
	 */
	this.exploredNodesCount = 0;
	
	/**
	 * @private
	 */
	this.closestNode = null;
};

/**
 * @public
 * @memberOf SHIVANSITE.AStar.prototype
 * @param {number} startX
 * @param {number} startY
 * @param {number} destinationX
 * @param {number} destinationY
 * @param {SHIVANSITE.AStarMap} aStarMap
 * @param {SHIVANSITE.DistanceCalculator} distanceCalculator
 * @returns {SHIVANSITE.AStarFloodResult}
 */
SHIVANSITE.AStar.prototype.flood = function(startX, startY, aStarMap,  distanceCalculator, maxDistance) {
	var st = new Date().getTime();
	
	if(maxDistance === undefined) {
		maxDistance = Number.POSITIVE_INFINITY;
	}
	
	if(startX < 0 || startX >= aStarMap.getWidth() ||
			startY < 0 || startY >= aStarMap.getHeight()) {
		throw "Start coordinates outside aStarMap";
	}	

	this.reinit(aStarMap, distanceCalculator);
	
	var openList = this.newOpenList(aStarMap);
	var closedList = this.newClosedList(aStarMap);
	
	var startNode = new SHIVANSITE.AStarNode(startX, startY, 0, 0);
	
	openList.add(startNode);
	
	var currentNode = null;	
	
	while(!openList.isEmpty()) {
		currentNode = openList.removeLowestFNode();       
		if(currentNode.g <= maxDistance) {
			closedList.add(currentNode.x, currentNode.y, currentNode);
			this.processSucessors(currentNode, openList, closedList, null, aStarMap);
		}
	}
    var computeTimeMs = new Date().getTime() - st;
    return new SHIVANSITE.AStarFloodResult(startX, startY, closedList, computeTimeMs);
};

/**
 * @public
 * @memberOf SHIVANSITE.AStar.prototype
 * @param {number} startX
 * @param {number} startY
 * @param {number} destinationX
 * @param {number} destinationY
 * @param {SHIVANSITE.AStarMap} aStarMap
 * @param {SHIVANSITE.DistanceCalculator} distanceCalculator
 * @returns {SHIVANSITE.AStarPath}
 */
SHIVANSITE.AStar.prototype.getPath = function(startX, startY, destinationX, destinationY, aStarMap,  distanceCalculator, maxNodesToExplore) {
	var st = new Date().getTime();
	
	var maxNodesToExplore = maxNodesToExplore === undefined ? Number.POSITIVE_INFINITY : maxNodesToExplore;
	if(maxNodesToExplore < 1) {
		throw "maxNodesToExplore must be >= 1";
	}
	
	if(startX < 0 || startX >= aStarMap.getWidth() ||
			startY < 0 || startY >= aStarMap.getHeight()) {
		throw "Start coordinates outside aStarMap";
	}
	
	if(destinationX < 0 || destinationX >= aStarMap.getWidth() ||
			destinationY < 0 || destinationY >= aStarMap.getHeight()) {
		throw "Destination coordinates outside aStarMap";
	}
	

	this.reinit(aStarMap, distanceCalculator);
	
	var openList = this.newOpenList(aStarMap);
	var closedList = this.newClosedList(aStarMap);
	
	var g = distanceCalculator.getDistance(new SHIVANSITE.AStarNode(destinationX,destinationY),new SHIVANSITE.AStarNode(startX,startY));
	var finish = new SHIVANSITE.AStarNode(destinationX,destinationY,g);
	
	var startNode = new SHIVANSITE.AStarNode(startX,startY,0,distanceCalculator.getDistance(new SHIVANSITE.AStarNode(startX,startY),finish));
	
	openList.add(startNode);
	
	var currentNode = null;	
	
	var arrivedAtDestination = false;
	while(!openList.isEmpty() && this.exploredNodesCount < maxNodesToExplore) {
		
		currentNode = openList.removeLowestFNode();
		
        if(finish.equals(currentNode)) {
        	arrivedAtDestination = true;
			break;
		}
        
        closedList.add(currentNode.x, currentNode.y, true);
        
        this.processSucessors(currentNode, openList, closedList, finish, aStarMap);
	}
	
	var endNode = null;
	if(arrivedAtDestination) {
		endNode = currentNode;
	} else if(this.closestNode != null) {
		endNode = this.closestNode;
	} else {
		endNode = startNode;
	}
	
	var pathNodesCount = this.getPathLength(endNode);
    var pathNodes = new Array();
    var tempNode = endNode;
    var i = 0;
    while(tempNode!=null) {
        pathNodes[pathNodesCount - 1 - i] = tempNode;
		i = i + 1;
        tempNode = tempNode.getParent();
    }

    var computeTimeMs = new Date().getTime() - st;
    return new SHIVANSITE.AStarPath(pathNodes,arrivedAtDestination,computeTimeMs,this.exploredNodesCount);
};

/**
 * @private
 */
SHIVANSITE.AStar.prototype.processSucessors = function(currentNode, openList, closedList, finish, aStarMap) {
	//Get all neighbors of analyzed node, that DO NOT contain impassable terrain:
	var neighbors = this.distanceCalculator.getNeighbors(currentNode, aStarMap);
	
	for(var i = 0; i < neighbors.length; i++) {
		var neighbor = neighbors[i];
		
		//if neighbor node is already IN THE CLOSED LIST, skip it:
        var closedNode = closedList.getValue(neighbor.x, neighbor.y);
        if (closedNode != null) {
        	continue;
        }
				
		//calculate the cost of getting to the neighbor THROUGH the currently analyzed node:
        var orthogonalNeighbor = currentNode.x - neighbor.x == 0 || currentNode.y - neighbor.y == 0; 
        var newG = currentNode.g + (orthogonalNeighbor ? this.distanceCalculator.orthogonalNeighborG : this.distanceCalculator.diagonalNeighborG);
		
		//if the neighbor is already IN THE OPEN LIST *AND* IT'S THERE WITH A LOWER G COST, skip it
		//as this means that this neighbor is already present in some other path (has some other parent) which is better than
		//what we're investigating right now:
		var neighborOlderState = openList.searchByPosition(neighbor.x, neighbor.y);
        if (neighborOlderState != null && neighborOlderState.g <= newG) {
        	continue;
        }

        //if we're here, that means that this neighbor represents a node worth investigating as a potential member of the best path

        /* 
		 * at this point we know that this neighbor is:
		 * - not on the closed list
		 * - is walkable (does not contain impassable terrain)
		 * - either
		 *    - not on the open list
		 *    - on the open list, but with a g cost higher than if it were to pass through the currently analyzed node 
		 *    (a.k.a.: if we replace it's current parent with the currently analyzed node, it will make for a less costly (shorter) path
		 * 
		 * Set it's g and h scores, set the currently analyzed node as its parent, and add it to the opened list:		 * 
		 */        
        neighbor.g = newG;
        //neighbor.h = finish == null ? 0 : this.distanceCalculator.getDistance(neighbor, finish);
        if(neighborOlderState == null) {
        	neighbor.h = finish == null ? 0 : this.distanceCalculator.getDistance(neighbor, finish);
        } else {
        	neighbor.h = neighborOlderState.h;
        }
        neighbor.f = neighbor.g + neighbor.h;
        neighbor.setParent(currentNode);
        
        if(this.closestNode == null || this.closestNode.h > neighbor.h) { 
			this.closestNode = neighbor;
		}
        
		//if this neighbor is present in the open list, but with a worse (higher) g score, then remove it from the opened list
		//this means that this neighbor has made it to the open list already, but with a parent which constitutes for a path which is
		//longer (costlier) than if it were to go through the currently analyzed node (have it as parent)
        if (neighborOlderState != null /* implicit: && neighborOlderState.g > newG */) {
        	//update existing node:
            openList.update(neighbor);
        } else {
        	//add new (not yet explored) node:
        	openList.add(neighbor); 
        	this.exploredNodesCount++;
        }       
	}
};

/**
 * @private
 */
SHIVANSITE.AStar.prototype.reinit = function(aStarMap,  distanceCalculator) {
	this.aStarMap = aStarMap;
	this.distanceCalculator = distanceCalculator;
	this.exploredNodesCount = 0;
	this.closestNode = null;
};

/**
 * @private
 */
SHIVANSITE.AStar.prototype.getPathLength = function(endNode) {
	var length = 0;
	var curNode = endNode;
	while(curNode != null) {
		length++;
		curNode = curNode.getParent();
	}
	return length;
};

/**
 * @private
 */
SHIVANSITE.AStar.prototype.newOpenList = function(aStarMap) {
	return new SHIVANSITE.AStarNodeHeap(aStarMap.getWidth(), aStarMap.getHeight());
};

/**
 * @private
 */
SHIVANSITE.AStar.prototype.newClosedList = function(aStarMap) {
	return new SHIVANSITE.AStarNodeHashmap();
};



/**
 * @class
 * @returns {SHIVANSITE.AStarChebyshevDistanceCalculator} 
 */
SHIVANSITE.AStarChebyshevDistanceCalculator = function(d, d2) {	
	this.d = d === undefined ? 10 : d;
	this.d2 = d === undefined ? 14 : d2;
	
	this.d2MinusTwoTimesD = (this.d2 - 2 * this.d);
	this.orthogonalNeighborG = this.d;
	this.diagonalNeighborG = this.d2;
	
};

SHIVANSITE.AStarChebyshevDistanceCalculator.prototype = {
		/**
		 * @public
		 * @memberOf DistanceCalculator.prototype
		 * @param {Node} n1 
		 * @param {Node} n2
		 * @return{Number} the distance between the nodes n1 and n2
		 */
		getDistance: function(n1, n2) {
			var dx = Math.abs(n1.x - n2.x);
		    var dy = Math.abs(n1.y - n2.y);
		    return this.d * (dx + dy) + this.d2MinusTwoTimesD * Math.min(dx, dy);
		},
		
		/**
		 * @public
		 * @memberOf DistanceCalculator.prototype
		 * @param {Node} node
		 * @param {SHIVANSITE.AStarMap} aStarMap
		 * @return {Array} - Array of Node objects representing the neighbors
		 */
		getNeighbors: function(node, aStarMap) {
			var neighbors = new Array();
			
			var yMinus1 = node.y - 1;
			var yPlus1 = node.y + 1;
			var xMinus1 = node.x - 1;
			var xPlus1 = node.x + 1;
			
			if(node.x > 0 && !aStarMap.isObstacle(xMinus1, node.y)) {
				var leftN = new SHIVANSITE.AStarNode(xMinus1, node.y);		
				leftN.sourceNeighborG = this.d;
				neighbors.push(leftN);
			}
			if(node.x < aStarMap.widthMinusOne && !aStarMap.isObstacle(xPlus1, node.y)) {
				var rightN = new SHIVANSITE.AStarNode(xPlus1, node.y);		
				rightN.sourceNeighborG = this.d;
				neighbors.push(rightN);
			}
			if(node.y > 0 && !aStarMap.isObstacle(node.x, yMinus1)) {
				var topN = new SHIVANSITE.AStarNode(node.x, yMinus1);	
				topN.sourceNeighborG = this.d;
				neighbors.push(topN);
			}
			if(node.y < aStarMap.heightMinusOne && !aStarMap.isObstacle(node.x, yPlus1)) {
				var bottomN = new SHIVANSITE.AStarNode(node.x, yPlus1);
				bottomN.sourceNeighborG = this.d;
				neighbors.push(bottomN);
			}
			
			if(node.x > 0 && node.y > 0 && !aStarMap.isObstacle(xMinus1, yMinus1)) {
				var leftUpperN = new SHIVANSITE.AStarNode(xMinus1, yMinus1);
				leftUpperN.sourceNeighborG = this.diagonalNeighborG;
				neighbors.push(leftUpperN);
			}			
			if(node.x > 0 && node.y < aStarMap.heightMinusOne && !aStarMap.isObstacle(xMinus1, yPlus1)) {
				var leftLowerN  = new SHIVANSITE.AStarNode(xMinus1, yPlus1);
				leftLowerN.sourceNeighborG = this.diagonalNeighborG;
				neighbors.push(leftLowerN);
			}
			if(node.x < aStarMap.widthMinusOne && node.y > 0 && !aStarMap.isObstacle(xPlus1, yMinus1)) {
				var rightUpperN = new SHIVANSITE.AStarNode(xPlus1, yMinus1);
				rightUpperN.sourceNeighborG = this.diagonalNeighborG;
				neighbors.push(rightUpperN);
			}
			if(node.x < aStarMap.widthMinusOne && node.y < aStarMap.heightMinusOne && !aStarMap.isObstacle(xPlus1, yPlus1)) {
				var rightLowerN  = new SHIVANSITE.AStarNode(xPlus1, yPlus1);
				rightLowerN.sourceNeighborG = this.diagonalNeighborG;
				neighbors.push(rightLowerN);
			}
			
			return neighbors;
		}
};



/**
 * @class
 * @returns {SHIVANSITE.AStarFloodResult}
 */
SHIVANSITE.AStarFloodResult = function(startX, startY, closedList, computeTimeMs) {
	this.startX = startX;
	this.startY = startY;
	this.closedList = closedList;
	this.computeTimeMs = computeTimeMs;
};

/**
 * @public
 */
SHIVANSITE.AStarFloodResult.prototype.getPath = function(destinationX, destinationY) {
	var endNode = this.closedList.getValue(destinationX, destinationY);
	if(endNode == null) {
		return null;
	}
	var pathNodesCount = this.getPathLength(endNode);
    var pathNodes = new Array();
    var tempNode = endNode;
    var i = 0;
    while(tempNode!=null) {
            pathNodes[pathNodesCount-1-i] = tempNode;
    		i = i+1;
            tempNode = tempNode.getParent();
    }
    return new SHIVANSITE.AStarPath(pathNodes, true, 0, 0);
};

/**
 * @private
 */
SHIVANSITE.AStarFloodResult.prototype.getPathLength = function(endNode) {
	var length = 0;
	var curNode = endNode;
	while(curNode != null) {
		length++;
		curNode = curNode.getParent();
	}
	return length;
};



/**
 * @class
 * @param {number} width
 * @param {number} height
 * @param {Array} somewhatPassablePoints - A multidimensional array (of arrays of 2 elements) containing x, y coordinates of each point
 * @param {Array} impassablePoints - A multidimensional array (of arrays of 2 elements) containing x, y coordinates of each point
 * @returns {SHIVANSITE.AStarMap}
 */
SHIVANSITE.AStarMap = function(width, height) {
	
	/**
	 * @private
	 */
	this.width = width;
	
	/**
	 * @private
	 */
	this.height = height;
	
	/**
	 * @private
	 */
	this.terrain = new SHIVANSITE.AStarNodeHashmap();
	
	this.widthMinusOne = this.width - 1;
	this.heightMinusOne = this.height - 1;
};

SHIVANSITE.AStarMap.prototype= {
		
		/**
		 * @public
		 * @memberOf SHIVANSITE.AStarMap.prototype
		 * @param {number} x
		 * @param {number} y
		 */
		setObstacle: function(x, y) {
			this.terrain.add(x, y, true);
		},
		
		/**
		 * @public
		 * @memberOf SHIVANSITE.AStarMap.prototype
		 * @param {number} x
		 * @param {number} y
		 */
		isObstacle: function(x, y) {
			return this.terrain.getValue(x, y) != null;
		},
		
		/**
		 * @public
		 * @memberOf SHIVANSITE.AStarMap.prototype
		 * @param {number} x
		 * @param {number} y
		 */
		clearObstacle: function(x, y) {
			this.terrain.remove(x, y);
		},
		
		/**
		 * @public
		 * @memberOf SHIVANSITE.AStarMap.prototype
		 * @param {number} x
		 * @param {number} y
		 */
		setObstacles: function(fromX, fromY, toX, toY) {
			for(var x = Math.min(fromX, toX); x <= Math.max(fromX, toX); x++) {
				for(var y = Math.min(fromY, toY); y <= Math.max(fromY, toY); y++) {
					this.setObstacle(x, y);
				}
			}
		},
		
		/**
		 * @public
		 * @memberOf SHIVANSITE.AStarMap.prototype
		 * @returns {number}
		 */
		getWidth: function() {
			return this.width;
		},
		
		/**
		 * @public
		 * @memberOf SHIVANSITE.AStarMap.prototype
		 * @returns {number}
		 */
		getHeight: function() {
			return this.height;
		},
		
		/**
		 * @public
		 * @memberOf SHIVANSITE.AStarMap.prototype
		 */
		toString: function() {
			return this.getStringRepresentation();
		},
		
		/**
		 * @private
		 * @memberOf SHIVANSITE.AStarMap.prototype
		 */
		getStringRepresentation: function() {
			var string = new String();
			for(var y = 0; y < this.height; y++) {
				for(var x = 0; x < this.width; x++) {
					if(this.isObstacle(x, y)) {
						string += "X";
					} else {
						string += "_";
					}
				}
				string += "|";
			}
			return string;
		}
};



/**
 * @class
 * @param {number} x
 * @param {number} y
 * @param {number} g
 * @param {number} h
 * @returns {SHIVANSITE.AStarNode}
 */
SHIVANSITE.AStarNode = function(x, y, g, h) {
	
	/**
	 * @public  
	 */
	this.x = x;
	
	/**
	 * @public  
	 */
	this.y = y;
	
	/**
	 * @public  
	 */
	this.g = g;
	
	/**
	 * @public  
	 */
	this.h = h;
	
	/**
	 * @public  
	 */
	this.f = g + h;
	
	/**
	 * @public
	 */
	this.openListHeapIndex = null;
	
	/**
	 * @private
	 */
	//this.hash = 31 * this.x + this.y;
//	this.hash = 255 + this.x + this.y;
	this.hash = 255 + 13 * this.x + 17 * this.y;
	
	/**
	 * @private
	 */
	this.parent = null;
};

SHIVANSITE.AStarNode.prototype = {
		
		/**
		 * @public 
		 * @memberOf SHIVANSITE.AStarNode.prototype
		 * @returns{SHIVANSITE.AStarNode} parent node
		 */
		getParent: function() {
			return this.parent;
		},
		
		
		/**
		 * @public 
		 * @memberOf SHIVANSITE.AStarNode.prototype
		 * @param{SHIVANSITE.AStarNode} node Parent
		 */
		setParent: function(node) {
			this.parent = node;
		},
		
		/**
		 * @public 
		 * @memberOf SHIVANSITE.AStarNode.prototype
		 * @param {SHIVANSITE.AStarNode} node Other node
		 * @returns {Boolean}
		 */
		equals: function(node) {
			return this.y == node.y && this.x == node.x;
		},
		
		/**
		 * @public 
		 * @memberOf SHIVANSITE.AStarNode.prototype
		 * @returns {string}
		 */
		toString: function() {
			return "["+this.x+":"+this.y+" "+this.f.toFixed(6)+"]";
		},
		
		/**
		 * @public 
		 * @memberOf SHIVANSITE.AStarNode.prototype
		 * @returns {string}
		 */
		toDetailedString: function() {
			return "["+this.x+":"+this.y+" "+this.g.toFixed(8)+" + "+this.h.toFixed(8) + " = " + this.f.toFixed(8);
		}
};



/**
 * @class
 * @param {Array} nodes - the nodes that make up this path, in the proper order
 * @param {boolean} reachesDestination
 * @param {number} computeTimeMs
 * @param {number} exploredNodesCount
 * @returns {SHIVANSITE.AStarPath}
 */
SHIVANSITE.AStarPath = function(nodes, reachesDestination, computeTimeMs, exploredNodesCount) {
	
	/**
	 * @public
	 */
	this.nodes = nodes;
	
	/**
	 * @public
	 */
	this.reachesDestination = reachesDestination;
	
	/**
	 * @public
	 */
	this.computeTimeMs = computeTimeMs;
		
	/**
	 * @public
	 */
	this.exploredNodesCount = exploredNodesCount;
};



/**
 * @class
 * @param {number} aStarMapWidth
 * @param {number} aStarMapHeight
 * @returns {SHIVANSITE.AStarNodeHeap}
 */
SHIVANSITE.AStarNodeHeap = function(aStarMapWidth, aStarMapHeight) {
	
	this.internalArray = new Array();	
	this.hashmap = new SHIVANSITE.AStarNodeHashmap();
};

SHIVANSITE.AStarNodeHeap.prototype = {
		
		/**
		 * @public
		 * @memberOf SHIVANSITE.AStarNodeHeap.prototype
		 * @param {SHIVANSITE.AStarNode} n
		 */
		add: function(n) {
			var index = this.internalArray.length;
			this.internalArray[index] = n;
			n.openListHeapIndex = index;
			this.hashmap.add(n.x, n.y, n);
			this.sortUp(index);
		},
		
		/**
		 * @public
		 */
		removeLowestFNode: function() {
			if(this.isEmpty()) {
				return null;
			}
			var n = this.internalArray[0];
			this.hashmap.remove(n.x, n.y);
			
			if(this.internalArray.length > 1) {
				var lastNodeOriginalIdx = this.internalArray.length - 1;
				var lastNode = this.internalArray[lastNodeOriginalIdx];
				this.internalArray[0] = lastNode;
				lastNode.openListHeapIndex = 0;				
				this.internalArray.splice(lastNodeOriginalIdx, 1);
				this.sortDown(0);
			} else if(this.internalArray.length == 1) {
				this.internalArray.splice(0, 1);
			}
			return n;
		},
		
		/**
		 * @public
		 */
		update: function(n) {
			var oldNode = this.hashmap.getValue(n.x, n.y);
			var oldNodeIdx = oldNode.openListHeapIndex;
			
			oldNode.g = n.g;
			oldNode.f = n.g + n.h;
			oldNode.parent = n.parent;
			
			//at this point, due to how AStar works, we know that the new node, n, has a lower F score than the old node its replacing because:
			//it has the same coordinates, so same h, and it has a lower g (otherwise we wouldn't update it).
			//so this node can at most be somewhere higher in the heap than its previous version
			this.sortUp(oldNodeIdx);
		},
		
		/**
		 * @public
		 * @memberOf SHIVANSITE.AStarNodeList.prototype
		 * @returns {SHIVANSITE.AStarNode}
		 */
		searchByPosition: function(x, y) {
			return this.hashmap.getValue(x, y);
		},
		
		/**
		 * @public
		 */
		getSize: function() {
			return this.internalArray.length;
		},
		
		/**
		 * @public
		 */
		isEmpty: function() {
			return this.internalArray.length == 0;
		},
		
		/**
		 * @public
		 */
		toString: function() {
			var string = "[" + this.internalArray.length + "]: ";
			for(var i = 0; i < this.internalArray.length; i++) {
				var n = this.internalArray[i];
				string += n.toString() + " | ";
			}
			return string;
		},
		
		/**
		 * @private
		 */
		sortDown: function(index) {
			var currentIndex = index;
			
			while(true) {			
				var doubleCurrentIndex = 2 * currentIndex;
				var leftChildIndex = doubleCurrentIndex + 1;
				var rightChildIndex = doubleCurrentIndex + 2;
				var swapIndex = null;
				
				if(leftChildIndex < this.internalArray.length) {
					swapIndex = leftChildIndex;
					
					if(rightChildIndex < this.internalArray.length) {
						if(this.internalArray[rightChildIndex].f < this.internalArray[leftChildIndex].f
								|| (this.internalArray[rightChildIndex].f == this.internalArray[leftChildIndex].f && this.internalArray[rightChildIndex].g > this.internalArray[leftChildIndex].g)) {
							swapIndex = rightChildIndex;
						}
					}
					
					if(this.internalArray[swapIndex].f < this.internalArray[currentIndex].f
								|| (this.internalArray[swapIndex].f == this.internalArray[currentIndex].f && this.internalArray[swapIndex].g > this.internalArray[currentIndex].g)) {
						this.swap(swapIndex, currentIndex);
						currentIndex = swapIndex;
					} else {
						break;
					}
				} else {
					break;
				}
			}
		},
		
		/**
		 * @private
		 */
		sortUp: function(index) {
			var currentIndex = index;	
			var parentIndex = ~~((currentIndex - 1) / 2);
			
			while(true) {
				var currentNode = this.internalArray[currentIndex];
				var parentNode = this.internalArray[parentIndex];
				if(parentNode !== undefined && (parentNode.f > currentNode.f || (parentNode.f == currentNode.f && parentNode.g < currentNode.g))) {
					this.swap(parentIndex, currentIndex);
					currentIndex = parentIndex;
					parentIndex = ~~((currentIndex - 1) / 2);
				} else {
					break;
				}
			}
		},
		
		/**
		 * @private
		 */
		swap: function (index1, index2) {
			var oldNodeAtIndex1 = this.internalArray[index1];
			var oldNodeAtIndex2 = this.internalArray[index2];
			
			this.internalArray[index1] = oldNodeAtIndex2;
			this.internalArray[index2] = oldNodeAtIndex1;
			
			oldNodeAtIndex2.openListHeapIndex = index1;
			oldNodeAtIndex1.openListHeapIndex = index2;
		}
};



/**
 * @public
 * @class
 * @returns {SHIVANSITE.AStarNodeHashmap}
 */
SHIVANSITE.AStarNodeHashmap = function() {
	this.storage = new Array();
};

/**
 * @public
 */
SHIVANSITE.AStarNodeHashmap.prototype.add = function(x, y, value) {
	if(!this.storage[x]) {
		this.storage[x] = new Array();
	}
	this.storage[x][y] = value;
};

/**
 * @public
 */
SHIVANSITE.AStarNodeHashmap.prototype.getValue = function(x, y) {
	if(!this.storage[x]) {
		return null;
	}
	if(!this.storage[x][y]) {
		return null;
	}
	return this.storage[x][y];
};

/**
 * @public
 */
SHIVANSITE.AStarNodeHashmap.prototype.remove = function(x, y) {
	if(!this.storage[x]) {
		return;
	}
	this.storage[x][y] = undefined;
};