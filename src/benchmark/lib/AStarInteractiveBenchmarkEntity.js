var SHIVANSITE = SHIVANSITE || {};

/**
 * @public
 * @class
 * @returns {SHIVANSITE.AStarInteractiveBenchmarkEntity}
 */
SHIVANSITE.AStarInteractiveBenchmarkEntity = function(cellSize, aStarMap, distanceCalculator, maxNodesToExplore, mapImg, showObstacleCells, allowObstacleEditing) {
	LS_GAME_LOOP.Entity.call(this, 0, 0, aStarMap.getWidth() * cellSize, aStarMap.getHeight() * cellSize, 10);
	
	this.cellSize = cellSize;
	this.aStarMap = aStarMap;
	this.distanceCalculator = distanceCalculator;
	this.maxNodesToExplore = maxNodesToExplore;
	this.mapImg = mapImg;
	this.showObstacleCells = showObstacleCells;
	this.allowObstacleEditing = allowObstacleEditing;
	
	this.aStar = new SHIVANSITE.AStar();		 
	
	this.cellColumns = aStarMap.getWidth();
	this.cellRows = aStarMap.getHeight();
	
	this.mouseOverCellX = null;
	this.mouseOverCellY = null;
	
	this.leftMouseButtonDown = false;
	this.rightMouseButtonDown = false;
	
	this.lastMouseDownX = null;
	this.lastMouseDownY = null;
	
	this.brushRadius = 3;
	
	this.pathStartsCellsCoordinates = new Array();
	this.pathEndCellX = null;
	this.pathEndCellY = null;
	
	this.recalculatePathCounter = 0;
	
	this.aStarPaths = new Array();
	
	this.lastPathsRecalculationTime = null;
		
	this.totalPathComputeTimeMs = 0;
	this.averagePathComputeTimeMs = null;
	
	this.pathCellInsetPercent = 0.35;
	
	this.thatsAnObstacleDisplayMessageX = null;
	this.thatsAnObstacleDisplayMessageY = null;
	this.thatsAnObstacleDisplayMessageCount = 0;
};

SHIVANSITE.AStarInteractiveBenchmarkEntity.prototype = Object.create(LS_GAME_LOOP.Entity.prototype);
SHIVANSITE.AStarInteractiveBenchmarkEntity.prototype.constructor = SHIVANSITE.AStarInteractiveBenchmarkEntity;

/**
 * @public
 */
SHIVANSITE.AStarInteractiveBenchmarkEntity.prototype.processInput = function(inputEvent) {
	if(inputEvent.type === "mouseout") {
		this.leftMouseButtonDown = false;
		this.rightMouseButtonDown = false;
	}
	
	if(inputEvent.type === "mousewheel") {		
		if(inputEvent.mouseWheelDeltaY > 0) {
			this.brushRadius--;
			if(this.brushRadius < 0) {
				this.brushRadius = 0;
			}
		}
		
		if(inputEvent.mouseWheelDeltaY < 0) {
			this.brushRadius++;
			if(this.brushRadius > 10) {
				this.brushRadius = 10;
			}
		}
		
//		console.debug(this.aStarMap.toString());
	}
	
	if(inputEvent.type === "mousedown") {
		this.lastMouseDownX = inputEvent.canvasX;
		this.lastMouseDownY = inputEvent.canvasY;
		
		if(inputEvent.mouseButton == 0) {
			this.leftMouseButtonDown = true;
		} else if(inputEvent.mouseButton == 2) {
			this.rightMouseButtonDown = true;
		}
	}
	
	if(inputEvent.type === "mouseup") {		
		var nonDraggedClick = inputEvent.canvasX == this.lastMouseDownX && inputEvent.canvasY == this.lastMouseDownY;
		
		var clickedCellX = this.getCellCoordinate(inputEvent.canvasX);
		var clickedCellY = this.getCellCoordinate(inputEvent.canvasY);
				
		if(inputEvent.mouseButton == 0) {
			this.leftMouseButtonDown = false;
			if(nonDraggedClick) {
				if(!this.aStarMap.isObstacle(clickedCellX, clickedCellY)) {
					this.pathEndCellX = clickedCellX;
					this.pathEndCellY = clickedCellY;
					this.recalculatePathCounter++;
				} else {
					this.thatsAnObstacleDisplayMessageX = inputEvent.canvasX;
					this.thatsAnObstacleDisplayMessageY = inputEvent.canvasY;
					this.thatsAnObstacleDisplayMessageCount = 60;
					
				}
			}
		} else if(inputEvent.mouseButton == 2) {
			this.rightMouseButtonDown = false;
			if(nonDraggedClick) {
				if(!this.aStarMap.isObstacle(clickedCellX, clickedCellY)) {
					this.pathStartsCellsCoordinates.push([clickedCellX, clickedCellY]);
					this.recalculatePathCounter++;
				} else {
					this.thatsAnObstacleDisplayMessageX = inputEvent.canvasX;
					this.thatsAnObstacleDisplayMessageY = inputEvent.canvasY;
					this.thatsAnObstacleDisplayMessageCount = 60;
				}
			}
		}
	}
	
	if(inputEvent.type === "mousemove") {
		this.mouseOverCellX = this.getCellCoordinate(inputEvent.canvasX);
		this.mouseOverCellY = this.getCellCoordinate(inputEvent.canvasY);
		
		if(!(inputEvent.canvasX === this.lastMouseDownX && inputEvent.canvasY === inputEvent.canvasY)) {
			
			var recalculatePath = false;
			
			for(var x = this.mouseOverCellX - this.brushRadius; x <= this.mouseOverCellX + this.brushRadius; x++) {
				for(var y = this.mouseOverCellY - this.brushRadius; y <= this.mouseOverCellY + this.brushRadius; y++) {
					if(x >= 0 && x < this.cellColumns &&
						y >= 0 && y < this.cellRows) {
						
						if(this.leftMouseButtonDown) {
							if(this.allowObstacleEditing) {
								this.aStarMap.setObstacle(x, y);
								recalculatePath = true;
							}
						} else if(this.rightMouseButtonDown) {
							if(this.allowObstacleEditing) {
								this.aStarMap.clearObstacle(x, y);
								recalculatePath = true;
							}
							if(this.pathEndCellX == x && this.pathEndCellY == y) {
								this.pathEndCellX = null;
								this.pathEndCellY = null;
							}
							
							var pathStartsCellsCoordinatesIndicesToRemove = new Array();
							for(var i = 0; i < this.pathStartsCellsCoordinates.length; i++) {
								if(this.pathStartsCellsCoordinates[i][0] == x && this.pathStartsCellsCoordinates[i][1] == y) {
									pathStartsCellsCoordinatesIndicesToRemove.push(i);
								} 
							}
							while(pathStartsCellsCoordinatesIndicesToRemove.length > 0) {
								recalculatePath = true;
								this.pathStartsCellsCoordinates.splice(pathStartsCellsCoordinatesIndicesToRemove.pop(), 1);
							}
						}
					}
				}
			}
			if(recalculatePath) {
				this.recalculatePathCounter++;
			}
		}
	}
};

/**
 * @public
 */
SHIVANSITE.AStarInteractiveBenchmarkEntity.prototype.updateState = function() {	
	if(this.pathEndCellX == null ||  this.pathEndCellY == null) {
		this.aStarPaths = new Array();
		return;
	}
	
	if(this.recalculatePathCounter > 0) {
		var now = new Date().getTime();
		if(this.lastPathsRecalculationTime != null && now - this.lastPathsRecalculationTime < 650 /* ms. */) {
			return;
		}
		
		this.aStarPaths = new Array();
		this.totalPathComputeTimeMs = 0;
		for(var i = 0; i < this.pathStartsCellsCoordinates.length; i++) {
			var startX = this.pathStartsCellsCoordinates[i][0];
			var startY = this.pathStartsCellsCoordinates[i][1];
			
			var aStarPath = this.aStar.getPath(startX, startY, this.pathEndCellX, this.pathEndCellY, this.aStarMap, this.distanceCalculator, this.maxNodesToExplore);
			this.totalPathComputeTimeMs += aStarPath.computeTimeMs;
			this.aStarPaths.push(aStarPath);
		}
		
		if(this.aStarPaths.length > 0) {
			this.averagePathComputeTimeMs = Math.round(this.totalPathComputeTimeMs / this.aStarPaths.length);
		} else {
			this.averagePathComputeTimeMs = 0;
		}
		
		this.recalculatePathCounter = 0;
		this.lastPathsRecalculationTime = new Date().getTime();
	}
};

/**
 * @public
 * @param {CanvasRenderingContext2D} context2d
 */
SHIVANSITE.AStarInteractiveBenchmarkEntity.prototype.updateGraphics = function(context2d) {
	this.drawMapImg(context2d);
	this.drawMapObstacles(context2d);	
	this.drawAStarPaths(context2d);
	this.drawPathsStartsAndEnd(context2d);
	this.drawAStarPathsTime(context2d);
	this.drawThatsAnObstacleMessage(context2d);
	this.drawMouseHighlightedCell(context2d);
};

/**
 * @private
 */
SHIVANSITE.AStarInteractiveBenchmarkEntity.prototype.drawMapImg = function(context2d) {
	if(this.mapImg === undefined || this.mapImg == null) {
		return;
	}
	
	context2d.drawImage(this.mapImg, 0, 0, this.cellColumns * this.cellSize, this.cellRows * this.cellSize);
};


/**
 * @private
 */
SHIVANSITE.AStarInteractiveBenchmarkEntity.prototype.drawMapObstacles = function(context2d) {
	if(!this.showObstacleCells) {
		return;
	}
	
	for(var x = 0; x < this.cellColumns; x++) {
		for(var y = 0; y < this.cellRows; y++) {
			if(this.aStarMap.isObstacle(x, y)) {
				this.drawColoredCell(context2d, x, y, "black");
			}
		}
	}
};

/**
 * @private
 */
SHIVANSITE.AStarInteractiveBenchmarkEntity.prototype.drawMouseHighlightedCell = function(context2d) {
	if(this.mouseOverCellX != null && this.mouseOverCellY != null) {
		this.drawColoredCell(context2d, this.mouseOverCellX, this.mouseOverCellY, "orange");
		this.drawEmptySquare(context2d, (this.mouseOverCellX  - this.brushRadius) * this.cellSize, 
				(this.mouseOverCellY  - this.brushRadius) * this.cellSize, 
				(2 * this.brushRadius + 1) * this.cellSize, 
				(2 * this.brushRadius + 1) * this.cellSize, 
				"orange");
	}
};

/**
 * @private
 */
SHIVANSITE.AStarInteractiveBenchmarkEntity.prototype.drawPathsStartsAndEnd = function(context2d) {
	for(var i = 0; i < this.pathStartsCellsCoordinates.length;i++) {
		var pathStartCellCoordinates = this.pathStartsCellsCoordinates[i];
		this.drawColoredCell(context2d, pathStartCellCoordinates[0], pathStartCellCoordinates[1], "rgb(255, 0, 0)");
		this.drawEmptyCircle(context2d, 
				"rgb(150, 0, 0)", 
				pathStartCellCoordinates[0] * this.cellSize + this.cellSize / 2, 
				pathStartCellCoordinates[1] * this.cellSize + this.cellSize / 2, 
				5, 
				1);
	}
	
	if(this.pathEndCellX != null && this.pathEndCellY != null) {
		this.drawColoredCell(context2d, this.pathEndCellX, this.pathEndCellY, "rgb(0, 0, 255)");
		this.drawEmptyCircle(context2d, 
				"rgb(0, 0, 150)", 
				this.pathEndCellX * this.cellSize + this.cellSize / 2, 
				this.pathEndCellY * this.cellSize + this.cellSize / 2, 
				5, 
				1);
	}
};

/**
 * @private
 */
SHIVANSITE.AStarInteractiveBenchmarkEntity.prototype.drawAStarPaths = function(context2d) {
	for(var i = 0; i < this.aStarPaths.length; i++) {
		var path = this.aStarPaths[i];
		for(var j = 0; j < path.nodes.length; j++) {
			var node = path.nodes[j];
			if(path.reachesDestination) {
				this.drawColoredCell(context2d, node.x, node.y, "rgb(255, 191, 0)", this.pathCellInsetPercent);
			} else {
				this.drawColoredCell(context2d, node.x, node.y, "rgb(255, 0, 255)", this.pathCellInsetPercent);
			}
		}
	}
};

/**
 * @private
 */
SHIVANSITE.AStarInteractiveBenchmarkEntity.prototype.drawAStarPathsTime = function(context2d) {
	if(this.pathEndCellX != null && this.pathEndCellX != null) {
		
		var msg = this.totalPathComputeTimeMs + " ms / " + this.aStarPaths.length + " paths = " + this.averagePathComputeTimeMs + " ms";
		
		context2d.font = "20px edit-undo-brk";
		context2d.fillStyle = "black";
		context2d.fillText(msg, Math.round(this.pathEndCellX * this.cellSize), Math.round(this.pathEndCellY * this.cellSize));
		context2d.strokeStyle = "orange";
		context2d.lineWidth = 1.5;
		context2d.strokeText(msg, Math.round(this.pathEndCellX * this.cellSize), Math.round(this.pathEndCellY * this.cellSize));
	}
};

/**
 * @private
 */
SHIVANSITE.AStarInteractiveBenchmarkEntity.prototype.drawThatsAnObstacleMessage = function(context2d) {
	if(this.thatsAnObstacleDisplayMessageCount > 0) {
		
		var msg = "That's an obstacle!";
		
		context2d.font = "25px edit-undo-brk";
		context2d.fillStyle = "black";
		context2d.fillText(msg, this.thatsAnObstacleDisplayMessageX, this.thatsAnObstacleDisplayMessageY);
		context2d.strokeStyle = "red";
		context2d.lineWidth = 1.5;
		context2d.strokeText(msg, this.thatsAnObstacleDisplayMessageX, this.thatsAnObstacleDisplayMessageY);
		
		this.thatsAnObstacleDisplayMessageCount--;
	}
};

/**
 * @private
 */
SHIVANSITE.AStarInteractiveBenchmarkEntity.prototype.drawColoredCell = function(context2d, cellX, cellY, color, insetPercent) {
	insetPercent = insetPercent || 0;
	var insetValue = this.cellSize * insetPercent;
	context2d.fillStyle = color;
	context2d.fillRect(cellX * this.cellSize + insetValue, 
			cellY * this.cellSize + insetValue, 
			this.cellSize - (2 * insetValue), 
			this.cellSize - (2 * insetValue));
};

/**
 * @private
 */
SHIVANSITE.AStarInteractiveBenchmarkEntity.prototype.drawEmptySquare = function(context2d, x, y, width, height, color) {
	context2d.strokeStyle = color;
	context2d.strokeRect(x, y, width, height);
};

/**
 * @private
 */
SHIVANSITE.AStarInteractiveBenchmarkEntity.prototype.drawEmptyCircle = function(context2d, color, x, y, radius, lineWidth) {
	context2d.strokeStyle = color;
	context2d.lineWidth = lineWidth || 1;
	context2d.beginPath();
	context2d.arc(x, y, radius, 0, 2*Math.PI);
	context2d.stroke();
};

/**
 * @private
 */
SHIVANSITE.AStarInteractiveBenchmarkEntity.prototype.getCellCoordinate = function(absoluteCoordinate) {
	return Math.floor(absoluteCoordinate / this.cellSize);
};
